# MIRA-CHALLENGE

A API consiste em um CRUD de pessoas (usuários)

O arquivo DBSeeder já cria automaticamente o document do banco de dados e insere dados ficticios.

O projeto base está configurado para rodar na porta localhost:8085

Para executar as requisições, utilize Postman ou curl.

### Pré requisitos para execução local
```
Java Version "1.8.0_191"
Apache Maven 3.5.2
Docker
```
### Execução local

	Para executar o software execute o arquivo br.com.pessoa.CrudApplication ou 
	rode o comando mvn install na pasta raiz do projeto depois execute o comando java -jar mira-challenge-1.0.0.jar
	que encontra-se no diretório /target
	
### API publicada na Google Cloud Platform (Kubernetes Engine) 
	A aplicação também foi publicada no Google Cloud Platform utilizando o Kubernetes Engine, abaixo as URLs
	
	GET findAll - http://35.202.65.33:8085/pessoas/
	GET findById - http://35.202.65.33:8085/pessoas/id/{id}
	GET findByNome - http://35.202.65.33:8085/pessoas/nome/{nome}
	GET findByCpf - http://35.202.65.33:8085/pessoas/sobrenome/{sobrenome}
	
	POST save - http://35.202.65.33:8085/pessoas/
	POST saveAll - http://35.202.65.33:8085/pessoas/bulk
	
	DELETE delete - http://35.202.65.33:8085/pessoas/{id}

### Instalação

Para gerar um pacote e fazer upload em um servidor de aplicação faça o seguinte comando na pasta raiz do projeto

```
mvn install
```

### Tecnologias utilizadas

- Java 8
- Spring Boot 2.1.1.RELEASE
- Model Mapper 2.0.0
- Lombok 1.18.2
- Mockito 2.21.0

### Banco de dados
- MongoDB (banco de dados orientado a documentos)

### Ambiente de desenvolvimento
- Ubuntu 18.04.1 LTS

### IDE
- Intellij IDEA 2018.3.2

### Observações
Não é necessario a criação de banco de dados local, o banco encontra-se no MongoDB Atlas.

