package br.com.pessoa.test;

import br.com.pessoa.controller.PessoaController;
import br.com.pessoa.dto.PessoaDTO;
import br.com.pessoa.model.Pessoa;
import br.com.pessoa.service.PessoaService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(PessoaController.class)
public class PessoaTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    private PessoaService pessoaService;


    @Test
    public void whenFindById_thenReturnIfIsEqual() throws Exception {
        when(pessoaService.findById("1")).thenReturn(ResponseEntity.ok(objectMapper.writeValueAsString(createPessoa())));

        mockMvc.perform(get("/pessoas/id/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is("1")))
                .andExpect(jsonPath("$.nome", Matchers.is("Danilo")))
                .andExpect(jsonPath("$.sobrenome", Matchers.is("Silva")))
                .andExpect(jsonPath("$.cpf", Matchers.is("378-272-658-80")))
                .andExpect(jsonPath("$.dataNascimento", Matchers.is("1990-07-17")))
                .andExpect(jsonPath("$.endereco", Matchers.is("Rua Catuti, 21")))
                .andExpect(jsonPath("$.telefone", Matchers.is(Arrays.asList("977993224"))))
                .andExpect(jsonPath("$.ativo", Matchers.is(true)))
                .andExpect(jsonPath("$.*", Matchers.hasSize(8)));
    }

    @Test
    public void givenPost_thenReturnIfIsOk() throws Exception {
        PessoaDTO pessoaDTO = createPessoaDTO();
        pessoaDTO.setId("1");
        pessoaDTO.setEndereco("Rua Diacir, 777");
        when(pessoaService.save(pessoaDTO)).thenReturn(ResponseEntity.ok(objectMapper.writeValueAsString(createPessoaDTO())));

        mockMvc.perform(post("/pessoas")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(pessoaDTO)))
                .andExpect(status().isOk());
    }


    @Test
    public void givenPut_thenReturnIfIsOk() throws Exception {
        PessoaDTO pessoaDTO = createPessoaDTO();
        pessoaDTO.setId(null);
        when(pessoaService.update(pessoaDTO)).thenReturn(ResponseEntity.ok(objectMapper.writeValueAsString(pessoaDTO)));

        mockMvc.perform(put("/pessoas")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(pessoaDTO)))
                .andExpect(status().isOk());
    }

    @Test
    public void givenDelete_thenReturnIfIsOk() throws Exception {
        when(pessoaService.delete("1")).thenReturn(ResponseEntity.ok(objectMapper.writeValueAsString("1")));

        mockMvc.perform(delete("/pessoas/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString("1")))
                .andExpect(status().isOk());
    }

    public Pessoa createPessoa() {
        Pessoa pessoa = new Pessoa();
        pessoa.setId("1");
        pessoa.setNome("Danilo");
        pessoa.setSobrenome("Silva");
        pessoa.setCpf("378-272-658-80");
        LocalDate localDate = LocalDate.of(1990, 07, 17);
        pessoa.setDataNascimento(localDate);
        pessoa.setEndereco("Rua Catuti, 21");
        pessoa.setTelefone(Arrays.asList("977993224"));
        pessoa.setAtivo(Boolean.TRUE);
        return pessoa;
    }

    public PessoaDTO createPessoaDTO() {
        PessoaDTO pessoaDTO = new PessoaDTO();
        pessoaDTO.setNome("Danilo");
        pessoaDTO.setSobrenome("Silva");
        pessoaDTO.setCpf("378-272-658-80");
        LocalDate localDate = LocalDate.of(1990, 07, 17);
        pessoaDTO.setDataNascimento(localDate);
        pessoaDTO.setEndereco("Rua Catuti, 21");
        pessoaDTO.setTelefone(Arrays.asList("977993224"));
        pessoaDTO.setAtivo(Boolean.TRUE);
        return pessoaDTO;
    }
}
