package br.com.pessoa.repository;

import br.com.pessoa.model.Pessoa;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PessoaRepository extends MongoRepository<Pessoa, String> {
    Optional<Pessoa> findByNome(String s);

    @Query(value = "{'sobrenome': {$regex : ?0}}")
    Optional<Pessoa> findBySobrenome(String s);
    Optional<Pessoa> findByCpf(String s);
    List<Pessoa> findByAtivo(Boolean b);


}
