package br.com.pessoa.service;

import br.com.pessoa.dto.PessoaDTO;
import br.com.pessoa.exceptions.ParametroObrigatorioException;
import br.com.pessoa.model.Pessoa;
import br.com.pessoa.repository.PessoaRepository;
import org.modelmapper.MappingException;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.modelmapper.spi.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PessoaService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PessoaRepository pessoaRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(PessoaService.class);


    public ResponseEntity getAll() {
        List<Pessoa> pessoas = pessoaRepository.findByAtivo(Boolean.TRUE);
        if (pessoas.isEmpty()) {
            LOGGER.error("Nenhuma pessoa encontrada");
            return ResponseEntity.badRequest().body("Nenhuma pessoa encontrada");
        }
        return ResponseEntity.ok(pessoas);
    }

    public ResponseEntity save(PessoaDTO pessoaDTO) throws ParametroObrigatorioException {
        if (pessoaDTO.getId() != null) {
            throw new ParametroObrigatorioException("Você não pode indicar um id, o id é gerado automaticamente");
        }

        pessoaDTO.setId(UUID.randomUUID().toString());
        Pessoa pessoa = convertToEntity(pessoaDTO);

        try {
            pessoaRepository.save(pessoa);
            return ResponseEntity.ok("Pessoa cadastrada com sucesso.");
        } catch (Exception e) {
            LOGGER.error("Falha ao inserir uma pessoa, error: ".concat(e.getMessage()));
            return ResponseEntity.badRequest().body("Erro ao inserir uma pessoa.");
        }
    }


    public ResponseEntity saveAll(List<PessoaDTO> pessoasDTO) {
        pessoasDTO.forEach(p -> p.setId(UUID.randomUUID().toString()));
        List<Pessoa> pessoas = convertToEntityList(pessoasDTO);

        try {
            pessoaRepository.saveAll(pessoas);
            return ResponseEntity.ok("Pessoas cadastradas com sucesso.");
        } catch (Exception e) {
            LOGGER.error("Falha ao inserir varias pessoas, error: ".concat(e.getMessage()));
            return ResponseEntity.badRequest().body("Erro ao inserir varias pessoas.");
        }
    }

    public ResponseEntity update(PessoaDTO pessoaDTO) throws ParametroObrigatorioException {
        if (pessoaDTO.getId() == null) {
            throw new ParametroObrigatorioException("Por favor, selecione uma pessoa.");
        }

        Pessoa pessoa = convertToEntity(pessoaDTO);
        try {
            pessoaRepository.save(pessoa);
            return ResponseEntity.ok("Pessoa atualizada com sucesso.");
        } catch (Exception e) {
            LOGGER.error("Falha ao inserir uma pessoa, error: ".concat(e.getMessage()));
            return ResponseEntity.badRequest().body("Erro ao atualizar uma pessoa.");
        }
    }

    public ResponseEntity<String> delete(String id) throws ParametroObrigatorioException {
        if (id == null) {
            throw new ParametroObrigatorioException("Selecione uma pessoa.");
        }
        Optional<Pessoa> optPessoa = pessoaRepository.findById(id);
        if (!optPessoa.isPresent()) {
            throw new ParametroObrigatorioException("Pessoa não lozalizada:".concat(id));
        }
        Pessoa pessoa = optPessoa.get();
        pessoa.setAtivo(Boolean.FALSE);

        try {
            pessoaRepository.save(pessoa);
            return ResponseEntity.ok("Pessoa foi deletada com sucesso.");
        } catch (Exception e) {
            LOGGER.error("Falha ao deletar, error: ".concat(e.getMessage()));
            return ResponseEntity.badRequest().body("Erro ao deletar uma pessoa.");
        }
    }

    public ResponseEntity findById(String id) throws ParametroObrigatorioException {
        Optional<Pessoa> pessoa = pessoaRepository.findById(id);
        if (!pessoa.isPresent()) {
            throw new ParametroObrigatorioException("Pessoa não lozalizada:".concat(id));
        }
        LOGGER.info("Pessoa ".concat(id).concat(" localizado."));
        return ResponseEntity.ok(pessoa);
    }

    public ResponseEntity findByNome(String nome) throws ParametroObrigatorioException {
        Optional<Pessoa> pessoa = pessoaRepository.findByNome(nome);
        if (!pessoa.isPresent()) {
            throw new ParametroObrigatorioException("Pessoa ".concat(nome).concat(" não localizado."));
        }
        LOGGER.info("Pessoa ".concat(nome).concat(" localizado."));
        return ResponseEntity.ok(pessoa);
    }

    public ResponseEntity findBySobrenome(String sobrenome) throws ParametroObrigatorioException {
        Optional<Pessoa> pessoa = pessoaRepository.findBySobrenome(sobrenome);
        if (!pessoa.isPresent()) {
            throw new ParametroObrigatorioException("Pessoa com sobrenome ".concat(sobrenome).concat(" não localizado(a)."));
        }
        LOGGER.info("Pessoa ".concat(sobrenome).concat(" localizado."));
        return ResponseEntity.ok(pessoa);
    }

    public ResponseEntity findByCpf(String cpf) throws ParametroObrigatorioException {
        Optional<Pessoa> pessoa = pessoaRepository.findByCpf(cpf);
        if (!pessoa.isPresent()) {
            throw new ParametroObrigatorioException("Pessoa com cpf ".concat(cpf).concat(" não lozalizado(a)."));
        }
        LOGGER.info("Pessoa ".concat(cpf).concat(" localizado."));
        return ResponseEntity.ok(pessoa);
    }

    public PessoaDTO convertToDTO(Pessoa Pessoa) throws MappingException {
        try {
            PessoaDTO clienteDTO = modelMapper.map(Pessoa, PessoaDTO.class);
            return clienteDTO;
        } catch (MappingException e) {
            LOGGER.error("Erro de-para objetos.", e.getMessage());
            throw new MappingException((List<ErrorMessage>) e.getErrorMessages());
        }
    }

    public Pessoa convertToEntity(PessoaDTO pessoaDTO) {
        try {
            Pessoa pessoa = modelMapper.map(pessoaDTO, Pessoa.class);
            return pessoa;
        } catch (MappingException e) {
            LOGGER.error("Erro de-para objetos.", e.getMessage());
            throw new MappingException((List<ErrorMessage>) e.getErrorMessages());
        }
    }


    public List<Pessoa> convertToEntityList(List<PessoaDTO> pessoasDTO) {
        try {
            Type targetListType = new TypeToken<List<Pessoa>>() {
            }.getType();

            List<Pessoa> pessoas = modelMapper.map(pessoasDTO, targetListType);
            return pessoas;
        } catch (MappingException e) {
            LOGGER.error("Erro de-para objetos.", e.getMessage());
            throw new MappingException((List<ErrorMessage>) e.getErrorMessages());
        }
    }


}
