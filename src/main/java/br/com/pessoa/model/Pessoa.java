package br.com.pessoa.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Document("Pessoa")
public class Pessoa {
    @Getter @Setter
    private String id;

    @Getter @Setter
    private String nome;

    @Getter @Setter
    private String sobrenome;

    @Getter @Setter
    private String cpf;

    @Getter @Setter
    private LocalDate dataNascimento;

    @Getter @Setter
    private String endereco;

    @Getter @Setter
    private List<String> telefone;

    @Getter @Setter
    private Boolean ativo;

}
