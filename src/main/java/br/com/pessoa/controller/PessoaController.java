package br.com.pessoa.controller;

import br.com.pessoa.dto.PessoaDTO;
import br.com.pessoa.exceptions.ParametroObrigatorioException;
import br.com.pessoa.repository.PessoaRepository;
import br.com.pessoa.service.PessoaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;


@RestController
@RequestMapping("/pessoas")
public class PessoaController implements Serializable {

    private static final long serialVersionUID = 6221889081690795087L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PessoaController.class);

    @Autowired
    private PessoaService pessoaService;

    @GetMapping
    public ResponseEntity findAll() {
        LOGGER.info("Buscando todas as pessoas...");
        return pessoaService.getAll();
    }

    @GetMapping("/id/{id}")
    public ResponseEntity findById(@PathVariable("id") String id) throws ParametroObrigatorioException {
        LOGGER.info("Buscando uma pessoa por id...");
        return pessoaService.findById(id);
    }

    @GetMapping("/nome/{nome}")
    public ResponseEntity findByNome(@PathVariable("nome") String nome) throws ParametroObrigatorioException {
        LOGGER.info("Buscando uma pessoa por nome...");
        return pessoaService.findByNome(nome);
    }

    @GetMapping("/sobrenome/{sobrenome}")
    public ResponseEntity findBySobrenome(@PathVariable("sobrenome") String sobrenome) throws ParametroObrigatorioException {
        LOGGER.info("Buscando uma pessoa por sobrenome...");
        return pessoaService.findBySobrenome(sobrenome);
    }

    @GetMapping("/cpf/{cpf}")
    public ResponseEntity findByCpf(@PathVariable("cpf") String cpf) throws ParametroObrigatorioException {
        LOGGER.info("Buscando uma pessoa por cpf...");
        return pessoaService.findByCpf(cpf);
    }

    @PostMapping
    public ResponseEntity save(@RequestBody PessoaDTO pessoaDTO) throws ParametroObrigatorioException {
        LOGGER.info("Inserindo nova pessoa...");
        return pessoaService.save(pessoaDTO);
    }

    @PostMapping("/bulk")
    public ResponseEntity saveAll(@RequestBody List<PessoaDTO> pessoaDTO) {
        LOGGER.info("Inserindo varias pessoas...");
        return pessoaService.saveAll(pessoaDTO);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody PessoaDTO pessoaDTO) throws ParametroObrigatorioException {
        LOGGER.info("Atualizando ".concat(pessoaDTO.getNome()).concat(" ..."));
        return pessoaService.update(pessoaDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") String id) throws ParametroObrigatorioException {
        LOGGER.info("Deletando pessoa...");
        return pessoaService.delete(id);
    }
}
