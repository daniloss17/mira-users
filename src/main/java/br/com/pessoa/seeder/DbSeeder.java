package br.com.pessoa.seeder;

import br.com.pessoa.model.Pessoa;
import br.com.pessoa.repository.PessoaRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Arrays;

@Component
public class DbSeeder implements CommandLineRunner {

    private PessoaRepository pessoaRepository;

    public DbSeeder(PessoaRepository pessoaRepository) {
        this.pessoaRepository = pessoaRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        pessoaRepository.deleteAll();
        {
            Pessoa pessoa = new Pessoa();
            pessoa.setId("1");
            pessoa.setAtivo(Boolean.TRUE);
            pessoa.setCpf("378-272-658-80");

            LocalDate localDate = LocalDate.of(1990, 07, 17);

            pessoa.setDataNascimento(localDate);

            pessoa.setEndereco("Rua Catuti, 21");
            pessoa.setTelefone(Arrays.asList("977993224"));
            pessoa.setNome("Danilo");
            pessoa.setSobrenome("Silva");

            pessoaRepository.insert(pessoa);
        }
        {
            Pessoa pessoa = new Pessoa();
            pessoa.setId("2");
            pessoa.setAtivo(Boolean.TRUE);
            pessoa.setCpf("379-570-658-80");
            pessoa.setDataNascimento(LocalDate.of(1988, 11, 24));
            pessoa.setEndereco("Rua Catuti, 21");
            pessoa.setTelefone(Arrays.asList("977993224", "977223332"));
            pessoa.setNome("Mari");
            pessoa.setSobrenome("Pereira");

            pessoaRepository.insert(pessoa);
        }

    }

}
