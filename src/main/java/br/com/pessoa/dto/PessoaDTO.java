package br.com.pessoa.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

public class PessoaDTO {
    @Getter @Setter
    private String id;

    @Getter @Setter
    private String nome;

    @Getter @Setter
    private String sobrenome;

    @Getter @Setter
    private String cpf;

    @Getter @Setter
    private LocalDate dataNascimento;

    @Getter @Setter
    private String endereco;

    @Getter @Setter
    private List<String> telefone;

    @Getter @Setter
    private Boolean ativo;

}
